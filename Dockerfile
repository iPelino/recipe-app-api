FROM python:3.9-alpine

ENV PYTHONUNBUFFERED 1


COPY ./requirements.txt /requirements.txt
RUN python -m pip install --upgrade pip
RUN /sbin/apk add --no-cache --virtual .deps gcc musl-dev
RUN pip install -r /requirements.txt

RUN mkdir /app
WORKDIR /app
COPY ./app /app

RUN adduser -D user
USER user



